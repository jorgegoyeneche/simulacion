from visual import * 
import math

alpha=1.0
d=2.1
h=0.1

sol = sphere(pos=(0,0,0),radius=0.5, color=color.orange)
tierra = sphere(pos = (0, 0, 0), radius=0.05, material=materials.earth)
tierra.trail = curve(color=color.blue)
asteroide = sphere(pos=(0,0,0), radius=0.02, color=color.red)
asteroide.trail = curve(color=color.yellow)

def rungeKutta(x, y, vx, vy, t):
    k1=h*vx
    l1=h*funcion1(x, y, vx, vy, t)
    q1=h*vy
    m1=h*funcion2(x, y, vx, vy, t)

    k2=h*(vx+l1/2)
    l2=h*funcion1(x+k1/2, y+q1/2, vx+l1/2, vy+m1/2, t+h/2)
    q2=h*(vy+m1/2)
    m2=h*funcion2(x+k1/2, y+q1/2, vx+l1/2, vy+m1/2, t+h/2)

    k3=h*(vx+l2/2)
    l3=h*funcion1(x+k2/2, y+q2/2, vx+l2/2, vy+m2/2, t+h/2)
    q3=h*(vy+m2/2);
    m3=h*funcion2(x+k2/2, y+q2/2, vx+l2/2, vy+m2/2, t+h/2)

    k4=h*(vx+l3)
    l4=h*funcion1(x+k3, y+q3, vx+l3, vy+m3, t+h)
    q4=h*(vy+m3)
    m4=h*funcion2(x+k3, y+q3, vx+l3, vy+m3, t+h)
    
    x+=(k1+2*k2+2*k3+k4)/6;
    vx+=(l1+2*l2+2*l3+l4)/6;
    y+=(q1+2*q2+2*q3+q4)/6;
    vy+=(m1+2*m2+2*m3+m4)/6;
    t+=h;
    
    return [x, y, vx, vy, t]
    

# valores para X
def funcion1(x,y,vx,vy,t):
    #return (-4*math.pi**2)*((x/(math.sqrt(((x**2)+(y**2))**3)))+((alpha*(x-d))/(math.sqrt((((x-d)**2)+(y**2))**3))))
    return (-x/(math.sqrt(((x**2)+(y**2))**3)))

# valores para Y
def funcion2(x, y, vx, vy, t):
    #return (-4*math.pi**2)*((y/(math.sqrt(((x**2)+(y**2))**3)))+((alpha*y)/(math.sqrt((((x-d)**2)+(y**2))**3))))
    return (-y/(math.sqrt(((x**2)+(y**2))**3)))

def main():
    tierra_x0=1.0
    tierra_vy0=1.2
    tierra_y0=0.0
    tierra_vx0=0.0
    tierra_t=0.0

    asteroide_x0=1.0
    asteroide_vy0=1.2
    asteroide_y0=0.0
    asteroide_vx0=0.0
    asteroide_t=0.0

    vectorTierra = []
    vectorAsteroide = []
    
    choque = 0
    velocidad,tamano,energiaCinetica, energiaImpacto,volumen,masa,densidad = 0,0,0,0,0,0,0
    velocidad = 1000*(input("ingrese la velocidad del asteroide(Km/s): "))
    tamano = input("Ingrese el diametro del asteroide(metros): ")
    densidad = input("Ingrese la densidad del asteroide(Kg/m3): ")
    volumen = 3.1416 * ((tamano/2)**3)
    volumen = tamano**3
    masa = densidad * volumen
    energiaCinetica = 0.5 * masa * (velocidad**2)
    energiaImpacto = energiaCinetica/420000000000000

    if (velocidad <= 20*1000):
        print("El asteroide choca con la tierra")
        print("la masa del asteroide es aproximadamente: " +str(masa/1000) +"Kg")
        print("la energia cinetica del asteroide es: " +str(energiaCinetica) +"Julios")
        print("La energia liberada por el asteroide al impactar es de: " +str(energiaImpacto) +" Megatones")
        print("Eqivalente a: " +str(energiaImpacto/(17.5/1000)) +" bombas de hiroshima o nagasaki")
        for i in range(1,1001):
            resultadoIteracion = rungeKutta(tierra_x0, tierra_y0, tierra_vx0, tierra_vy0, tierra_t)
            tierra_x0 = resultadoIteracion[0]
            tierra_y0 = resultadoIteracion[1]
            tierra_vx0 = resultadoIteracion[2]
            tierra_vy0 = resultadoIteracion[3]
            tierra_t = resultadoIteracion[4]

            resultadoIteracion = rungeKutta(asteroide_x0, asteroide_y0, asteroide_vx0, asteroide_vy0, asteroide_t)
            asteroide_x0 = resultadoIteracion[0]+0.01
            asteroide_y0 = resultadoIteracion[1]
            asteroide_vx0 = resultadoIteracion[2]
            asteroide_vy0 = resultadoIteracion[3]
            asteroide_t = resultadoIteracion[4]
    
            vectorTierra.append([tierra_x0, tierra_y0])
            vectorAsteroide.append([asteroide_x0, asteroide_y0])

        #calculo el punto en el que se chocan
        for i in range (0,len(vectorTierra)):
            if( (abs(vectorTierra[i][0]-vectorAsteroide[i][0]) < 0.0005) and (abs(vectorTierra[i][1]-vectorAsteroide[i][1]) < 0.0005) ):
                choque = i
                
        #muestro en vpython el choque
        for i in range (choque-200,choque-100):
            tierra.pos = (vectorTierra[i][0],vectorTierra[i][1],0)
            tierra.trail.append(pos=tierra.pos)
            asteroide.pos = (vectorAsteroide[i][0],vectorAsteroide[i][1],0)
            asteroide.trail.append(pos=asteroide.pos)
            rate(10)

    elif(velocidad > 20*1000 and velocidad <= 70*1000):
        print("El asteroide no choca la tierra")
        for i in range(1,1001):
            resultadoIteracion = rungeKutta(tierra_x0, tierra_y0, tierra_vx0, tierra_vy0, tierra_t)
            tierra_x0 = resultadoIteracion[0]
            tierra_y0 = resultadoIteracion[1]
            tierra_vx0 = resultadoIteracion[2]
            tierra_vy0 = resultadoIteracion[3]
            tierra_t = resultadoIteracion[4]

            resultadoIteracion = rungeKutta(asteroide_x0, asteroide_y0, asteroide_vx0, asteroide_vy0, asteroide_t)
            asteroide_x0 = resultadoIteracion[0]+0.05
            asteroide_y0 = resultadoIteracion[1]
            asteroide_vx0 = resultadoIteracion[2]
            asteroide_vy0 = resultadoIteracion[3]
            asteroide_t = resultadoIteracion[4]
    
            vectorTierra.append([tierra_x0, tierra_y0])
            vectorAsteroide.append([asteroide_x0, asteroide_y0])

        #muestro en vpython el choque
        for i in range (0,len(vectorTierra)):
            tierra.pos = (vectorTierra[i][0],vectorTierra[i][1],0)
            tierra.trail.append(pos=tierra.pos)
            asteroide.pos = (vectorAsteroide[i][0],vectorAsteroide[i][1],0)
            asteroide.trail.append(pos=asteroide.pos)
            rate(50)
    else:
        print("La velocidad del asteroide no permite que el asteroide tenga una orbita con respecto al sol")
        for i in range(1,1001):
            resultadoIteracion = rungeKutta(tierra_x0, tierra_y0, tierra_vx0, tierra_vy0, tierra_t)
            tierra_x0 = resultadoIteracion[0]
            tierra_y0 = resultadoIteracion[1]
            tierra_vx0 = resultadoIteracion[2]
            tierra_vy0 = resultadoIteracion[3]
            tierra_t = resultadoIteracion[4]

            resultadoIteracion = rungeKutta(asteroide_x0, asteroide_y0, asteroide_vx0, asteroide_vy0, asteroide_t)
            asteroide_x0 = resultadoIteracion[0]+0.1
            asteroide_y0 = resultadoIteracion[1]
            asteroide_vx0 = resultadoIteracion[2]
            asteroide_vy0 = resultadoIteracion[3]
            asteroide_t = resultadoIteracion[4]
    
            vectorTierra.append([tierra_x0, tierra_y0])
            vectorAsteroide.append([asteroide_x0, asteroide_y0])
            
        for i in range (0,len(vectorTierra)): 
            tierra.pos = (vectorTierra[i][0],vectorTierra[i][1],0)
            tierra.trail.append(pos=tierra.pos)
            asteroide.pos = (vectorAsteroide[i][0],vectorAsteroide[i][1],0)
            asteroide.trail.append(pos=asteroide.pos)
            rate(50)

if(__name__ == "__main__"):
    main()
